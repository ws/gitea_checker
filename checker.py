import requests
import random
import string

# Generate a random 2 character username
def generate_username():
  # Generate a random 2 character string using ascii letters
  return ''.join(random.choices(string.ascii_lowercase, k=2))

# Check if the username is available on gitea.com
def check_availability(username):
  # Send a GET request to https://gitea.com/{username}
  r = requests.get(f"https://gitea.com/{username}")
  
  # Return the HTTP status code
  return r.status_code

# Open a file to store the available usernames
with open("gitea_usernames_unreg.txt", "a+") as f:
  # Keep generating usernames and checking availability until we find an available one
  while True:
    username = generate_username()
    availability = check_availability(username)
    
    # If the username is available (HTTP status code is 200)
    if availability == 404:
      # Check if the username is already in the file
      f.seek(0)  # Go to the beginning of the file
      if username not in f.read():
        # Write the username to the file
        f.write(f"https://gitea.com/{username}\n")
        
        # Print a message to the console
        print(f"Found available username: {username}")
      else:
        # Print a message to the console
        print(f"Username {username} is already in the file")
    else:
      # Print a message to the console
      print(f"Username {username} is not available")
